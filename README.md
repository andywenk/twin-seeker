# TwinSeeker

![Twin Seeker](icons/TwinSeeker-Icon.png)

A simple Electron application for finding duplicate files recursively.

## Run the app

Install the node modules:

    ~ npm install

Run the app

    ~ npm start

## Build the App
    
Install the node modules:

    ~ npm install

Build the application for your actual OS:

    ~ npm run build

Build the application for Mac OS, Windows and Linux:

    ~ npm run build -- --windows --mac --linux

## Example

![Video](assets/Twin-Seeker.mp4)

## License

Copyright [2023] [Andy Wenk]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.





