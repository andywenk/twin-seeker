// Copyright [2023] [Andy Wenk]

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const { app, BrowserWindow, ipcMain, dialog } = require('electron');
const path = require('path');
const fs = require('fs');
const crypto = require('crypto');

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    icon: path.join(__dirname, '..', 'icons/TwinSeeker-Icon'),
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  });

  // mainWindow.loadFile('index.html');
  mainWindow.loadFile(path.join(__dirname, 'index.html'));
}

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

ipcMain.handle('select-directory', async () => {
  const result = await dialog.showOpenDialog(mainWindow, {
    properties: ['openDirectory']
  });

  if (!result.canceled) {
    return result.filePaths[0];
  }
  return null;
});

ipcMain.handle('find-duplicates', async (event, dirPath) => {
  const duplicates = await findDuplicateFiles(dirPath, event.sender);
  return duplicates;
});

ipcMain.on('exit-app', () => {
  app.quit();
});

ipcMain.handle('delete-duplicate', async (_, duplicatePath) => {
  try {
    await fs.promises.unlink(duplicatePath);
    return true;
  } catch (error) {
    console.error(`Error deleting duplicate: ${error}`);
    return false;
  }
});

async function findDuplicateFiles(dirPath, rendererWebContents) {
  const files = await getAllFilesRecursive(dirPath);
  const fileHashes = new Map();
  const duplicates = [];

  let processedFiles = 0;

  for (const file of files) {
    const fileHash = await hashFile(file);

    if (fileHashes.has(fileHash)) {
      duplicates.push({ original: fileHashes.get(fileHash), duplicate: file });
    } else {
      fileHashes.set(fileHash, file);
    }

    // Fortschrittsbalken aktualisieren
    processedFiles += 1;
    const progress = (processedFiles / files.length) * 100;
    rendererWebContents.send('progress-update', progress);
  }
  return duplicates;
}

async function getAllFilesRecursive(dirPath) {
  const entries = await fs.promises.readdir(dirPath, { withFileTypes: true });
  const filePaths = await Promise.all(
    entries.map(async (entry) => {
      const fullPath = path.join(dirPath, entry.name);
      if (entry.isDirectory()) {
        return getAllFilesRecursive(fullPath);
      } else {
        return fullPath;
      }
    })
  );

  return Array.prototype.concat(...filePaths);
}

function hashFile(filePath) {
  return new Promise((resolve, reject) => {
    const hash = crypto.createHash('sha1');
    const stream = fs.createReadStream(filePath);

    stream.on('data', (chunk) => {
      hash.update(chunk);
    });

    stream.on('end', () => {
      resolve(hash.digest('hex'));
    });

    stream.on('error', (error) => {
      reject(error);
    });
  });
}
