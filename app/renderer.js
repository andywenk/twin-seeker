// Copyright [2023] [Andy Wenk]

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const { ipcRenderer } = require('electron');
const path = require('path');
const selectDirectoryButton = document.getElementById('select-directory');
const selectedDirectory = document.getElementById('selected-directory');
const findDuplicatesButton = document.getElementById('find-duplicates');
const duplicateList = document.getElementById('duplicate-list');
const duplicateCount = document.getElementById('duplicate-count'); 
const progressBarContainer = document.getElementById('progress-container');
const progressBar = document.getElementById('progress-bar');
const exitButton = document.getElementById('exit-button');

selectDirectoryButton.addEventListener('click', async () => {
  duplicateList.innerHTML = '';
  showCountDuplicates(-1);
  const dirPath = await ipcRenderer.invoke('select-directory');
  duplicateCount.textContent = '';
  if (dirPath) {
    selectedDirectory.textContent = dirPath;
    findDuplicatesButton.removeAttribute('disabled');
  }
});

findDuplicatesButton.addEventListener('click', async () => {
  const dirPath = selectedDirectory.textContent;
  if (dirPath) {
    progressBarContainer.style.display = 'block'; // Fortschrittsbalken-Container einblenden
    progressBar.value = 0; // Fortschrittsbalken zurücksetzen

    const duplicates = await ipcRenderer.invoke('find-duplicates', dirPath);

    progressBarContainer.style.display = 'none'; // Fortschrittsbalken-Container ausblenden
    displayDuplicates(duplicates);
  }
});

function displayDuplicates(duplicates) {
  duplicateList.innerHTML = '';
  duplicates.forEach((duplicate) => {
    const listItem = document.createElement('tr');
    listItem.innerHTML = `<td><strong>Original:</strong>${path.basename(duplicate.original)}<br /><strong>Duplicate:</strong>${path.basename(duplicate.duplicate)}</td>`;
    
    const deleteButton = document.createElement('button');
    deleteButton.innerHTML = `<td>Löschen</td>`;
    deleteButton.addEventListener('click', async () => {
      await ipcRenderer.invoke('delete-duplicate', duplicate.duplicate);
      listItem.remove(); // ListItem aus der Liste entfernen
      updateDuplicateCount(); // Anzahl der verbleibenden Duplikate aktualisieren
    });

    listItem.appendChild(deleteButton);
    duplicateList.appendChild(listItem);
  });

  showCountDuplicates(duplicates);
  updateDuplicateCount();
}

function updateProgressBar(progress) {
  progressBar.value = progress;
}

// Anzahl der gefundenen Duplikate anzeigen
function showCountDuplicates(duplicates) {
  if (duplicates.length > 0) {
    duplicateCount.textContent = `${duplicates.length} Duplikat(e) gefunden`;
  } else if (duplicates === -1) {
    duplicateCount.textContent = '';
  } else {
    duplicateCount.textContent = 'Keine Duplikate gefunden';
  }
}

function updateDuplicateCount() {
  const count = document.querySelectorAll('#duplicate-list td').length;
  if (count > 0) {
    duplicateCount.textContent = `${count} Duplikat(e) gefunden`;
  } else {
    duplicateCount.textContent = 'Keine Duplikate gefunden';
  }
}


ipcRenderer.on('progress-update', (_, progress) => {
  updateProgressBar(progress);
});

exitButton.addEventListener('click', () => {
  ipcRenderer.send('exit-app');
});
